use std::{collections::HashMap, process::Command};

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, bail, Context, Result};
use itertools::Itertools;
use log::{info, warn};
use serde::Deserialize;

use crate::{
    build_dir,
    cmdline::{Args, SimulationArgs},
    cocotb::{list_failed_tests, TestCase},
    compiler_dir,
    config::Config,
    plugin::PluginList,
    spade::build_spade,
    spade::SpadecOutput,
    util::{files_in_dir, make_relative, needs_rebuild},
    venv_path,
};

/// Same format as `spade_compiler::name_dump`
#[derive(Deserialize)]
pub enum ItemKind {
    /// The item is a unit which is not generic and can thus easily be
    /// referred to
    Normal(String),
    /// The item exists, is a unit but is generic and there is therefore
    /// not an easy mapping between the path and name
    Generic,
    /// The item is a type
    Type,
}

/// The result of executing all the tests in a test bench file
pub struct TestFileResult {
    pub file: Utf8PathBuf,
    pub vcd_file: Utf8PathBuf,
    pub failed_tests: Vec<TestCase>,
}

pub struct SimulationResult {
    pub type_file: Utf8PathBuf,
    pub result: Vec<TestFileResult>, // List of failed tests (verilog, vcd file)
}

/// Initialises a venv and installs spade-python to it. Requires python and
/// pip to be available
fn setup_venv(root_dir: &Utf8Path, config: &Config) -> Result<()> {
    let venv_done_marker = build_dir(root_dir).join(".venv_done");

    if needs_rebuild(&venv_done_marker, &["swim.toml".into()])? {
        let result = Command::new("python3")
            .arg("-m")
            .arg("venv")
            .arg(&venv_path(root_dir))
            .status()?;

        if !result.success() {
            return Err(anyhow!("Failed to create python virtual envronment"));
        }

        let pip_command = format!(
            "pip3 install maturin {}",
            config
                .simulation_config()?
                .python_deps
                .iter()
                .flatten()
                .join(" ")
        );

        let maturin_install_result = do_in_venv(root_dir, &pip_command, &[])?;

        if !maturin_install_result.success() {
            bail!("Failed to install maturin");
        }

        std::fs::write(&venv_done_marker, "")
            .with_context(|| format!("Failed to write {venv_done_marker}"))?;
    }

    // NOTE: Maturin seems to rebuild even if no changes occur. This is annoying so we'll
    // manually do rebuild checking here by calling cargo build, and if the produced artefact is
    // newer than our marker, we rebuild the lib in the venv
    // lifeguard https://github.com/PyO3/maturin/issues/884
    // lifeguard https://github.com/PyO3/pyo3/issues/1708
    // if needs_rebuild("build/maturin_rebuild_tag")

    let crate_path = crate::spade::build_spade_python(root_dir, config)?;
    let compiler_dir = compiler_dir(root_dir, &config.compiler);

    let maturin_marker = build_dir(root_dir).join("maturin_done");
    if needs_rebuild(&maturin_marker, &[crate_path])? {
        let maturin_result = do_in_venv(
            root_dir,
            &format!("cd {compiler_dir}/spade-python && maturin develop"),
            &[],
        )
        .context("Failed to run maturin in venv")?;

        if !maturin_result.success() {
            bail!("Failed to build spade compiler python plugin")
        }

        std::fs::write(&maturin_marker, "")
            .with_context(|| format!("Failed to write {maturin_marker}"))?;
    }

    Ok(())
}

/// Runs the specified command string in a subshell in the specified venv
pub fn do_in_venv(
    root_dir: &Utf8Path,
    cmd_str: &str,
    extra_env: &[(String, String)],
) -> Result<std::process::ExitStatus> {
    let activate_path = venv_path(root_dir).join("bin/activate");

    let full = format!(". {activate_path} && {cmd_str}");

    Ok(Command::new("sh")
        .arg("-c")
        .arg(full)
        .envs(extra_env.into_iter().cloned())
        .status()?)
}

fn get_uut_name(tb: &Utf8Path) -> Result<String> {
    let file_content =
        std::fs::read_to_string(tb).with_context(|| format!("Failed to read {tb}"))?;

    let first_line = file_content
        .lines()
        .filter(|s| !s.is_empty())
        .next()
        .ok_or_else(|| anyhow!("{tb} is empty"))?
        .to_string();

    match first_line.chars().next() {
        Some('#') => {}
        Some(_) => bail!(
            "The first line of a test bench must be a comment specifying the module under test \n
            # top=path::to::uut

            {tb} started with {first_line}"
        ),
        None => unreachable!("We already made sure there is at least one non-empty line"),
    };

    // NOTE: This index operation is safe because we know the first byte is #, so
    // we can safely index
    let rest_line = &first_line
        .get(1..)
        .expect("Expected at least one line in tb");

    let split = rest_line.split("=").map(String::from).collect::<Vec<_>>();
    let (lhs, rhs) = match split.as_slice() {
        [lhs, rhs] => (lhs, rhs),
        _ => bail!("Expected {tb} to start with # top=path::to::uut. Got {first_line}"),
    };

    if lhs.trim() != "top" {
        bail!("{tb}:1: The first line of a test bench must be `top=path::to::uut`");
    }

    let top = rhs.trim();

    Ok(top.to_string())
}

/// Runs a cocotb test suite at the specified path. Returns an error if any
/// underlying command returns exit 1, and a list of failed test cases if test
/// running is successful, along with the vcd file to which the waves were dumped
pub fn run_cocotb(
    root_dir: &Utf8Path,
    tb: &Utf8Path,
    top: &str,
    spade_verilog: &Utf8Path,
    spade_state: &Utf8Path,
    spade_path: String,
    testcases: &[String],
    config: &Config,
) -> Result<(Vec<TestCase>, Utf8PathBuf)> {
    let global_extra_verilog = config.extra_verilog.clone().unwrap_or(vec![]);
    let global_extra_verilog_str = global_extra_verilog
        .iter()
        .map(|v| root_dir.join(format!("{v}")))
        .join(" ");

    let spade_src = root_dir.join(spade_verilog);

    let tb_stem = tb.file_stem().unwrap();

    // cocotb is annoying in at least 2 ways: it creates temproary files like __pycache__, vcd
    // files etc next to the python files, and it fails to re-compile the same file with different
    // configurations. To solve both those issues, we'll create a directory in our build directory,
    // copy the tb file in there and run the tests that way

    let sim_dir = build_dir(root_dir).join(tb_stem);
    std::fs::create_dir_all(&sim_dir)
        .with_context(|| format!("Failed to create dir {}", sim_dir))?;
    let actual_tb_path = sim_dir.join(tb.file_name().unwrap());
    std::fs::copy(&tb, &actual_tb_path)
        .with_context(|| format!("Failed to copy {tb} into {actual_tb_path}"))?;

    let vcd_filename = sim_dir.join(tb.file_name().unwrap()).with_extension("vcd");

    let testcases_str = testcases.join(",");

    let simulator = &config.simulation_config()?.simulator;
    let (extra_args, extra_plusargs, sim_args) = match simulator.as_str() {
        // FIXME -Wno-fatal is needed because we have a few warnings in the emitted code which
        // verilog catches
        // SYNTHESIS=1 ensures that we don't compile constructs not supported by verilator,
        // in particular: assertions which currently use #0, and the wave dump commands
        // used by other simulators
        "verilator" => ("-Wno-fatal", "+SYNTHESIS=1", ""),
        // -n tells vvp to exit on ctrl-c, rather than going into an interactive shell
        // which is inaccessible to us anyway.
        "icarus" => ("", "", "-n"),
        _ => ("", "", ""),
    };

    let make_cmd = format!(
        r#"
    make \
        -C {sim_dir} \
        -f $(cocotb-config --makefiles)/Makefile.sim \
        VERILOG_SOURCES='{spade_src} {global_extra_verilog_str}' \
        TOPLEVEL='{top}' \
        MODULE={tb_stem} \
        TESTCASE={testcases_str} \
        SIM={simulator} \
        EXTRA_ARGS={extra_args} \
        SIM_ARGS={sim_args} \
        -s \
        PLUSARGS="+TOP_MODULE={top} +VCD_FILENAME={vcd_filename} {extra_plusargs}"
    "#
    );

    let envs = [
        (
            "SWIM_SPADE_STATE".to_string(),
            root_dir.join(spade_state).to_string(),
        ),
        ("SWIM_UUT".to_string(), spade_path),
    ];

    do_in_venv(root_dir, &make_cmd, &envs)?;

    let result = list_failed_tests(&sim_dir.join("results.xml"))
        .map(|list| (list, make_relative(&tb.with_file_name(vcd_filename))));

    result
}

/// Simulates the source files throwing an error if compilation fails, or
/// if the tests fail.
///
/// Returns a list of vcd files which should have been generated. However, as it
/// is up to the test bench to actually generate the test files, they may not
/// actually exist after simulation
pub fn simulate(
    root_dir: &Utf8Path,
    compiler: &Utf8Path,
    args: &Args,
    sim_args: &SimulationArgs,
    config: &Config,
    plugins: &PluginList,
) -> Result<SimulationResult> {
    let SpadecOutput {
        verilog: spade_target,
        type_file,
        state_file,
        item_file,
    } = build_spade(root_dir, compiler, args, config, plugins)?;

    let item_list_str = std::fs::read_to_string(&item_file)
        .with_context(|| format!("Failed to read item list at {item_file}"))?;
    let item_list = ron::from_str::<HashMap<Vec<String>, ItemKind>>(&item_list_str)
        .with_context(|| format!("Failed to decode {item_file} as ron"))?;

    setup_venv(root_dir, config)?;

    let simulation_config = config.simulation_config()?;

    let testbenches = files_in_dir(&simulation_config.testbench_dir, "py")?;

    let mut result = vec![];

    for tb in testbenches {
        if let Some(filter) = &sim_args.testbench_filter {
            if !tb.as_str().contains(filter) {
                continue;
            }
        }

        let top_name = get_uut_name(&tb)?;

        let mut top_parts = top_name
            .split("::")
            .map(|ident| ident.to_string())
            .collect::<Vec<_>>();

        // We need to insert proj:: for namespacing to work correctly
        top_parts.insert(0, "proj".to_string());

        let top_verilog_name = item_list
            .get(&top_parts)
            .ok_or_else(|| {
                // A common pitfall here is most likely to try `a` when you in fact
                // mean `main::a`. Try to be helpful if that is the case
                top_parts.insert(1, "main".to_string());

                if item_list.get(&top_parts).is_some() {
                    anyhow!("Failed to find module {top_name}. Did you mean main::{top_name}?")
                } else {
                    anyhow!("No item named {top_name}")
                }
            })
            .and_then(|kind| match kind {
                ItemKind::Normal(name) => Ok(name),
                ItemKind::Generic => Err(anyhow!(
                    "{top_name} is generic which is currently unsupported in test benches"
                )),
                ItemKind::Type => Err(anyhow!("{top_name} is a type")),
            })
            .context("In {tb}")?;

        let (failed_tests, vcd_file) = run_cocotb(
            root_dir,
            &tb,
            &format!("{top_verilog_name}"),
            &spade_target,
            &state_file,
            top_parts.join("::"),
            &sim_args.testcases,
            config,
        )?;

        let relative_tb = simulation_config
            .testbench_dir_original
            .join(tb.file_name().unwrap());

        result.push(TestFileResult {
            file: relative_tb,
            vcd_file,
            failed_tests,
        })
    }

    Ok(SimulationResult { type_file, result })
}

/// Translates variables in the provided VCD files according to the types defined
/// in type_file. A VCD file which does
/// not exist prints a warning but does not return Err since the test bench might
/// not correctly generate the VCD file
/// Returns a map of vcd file to translated vcd file
pub fn translate_vcds(
    vcd_translator: &Utf8Path,
    vcd_files: Vec<Utf8PathBuf>,
    type_file: Utf8PathBuf,
) -> Result<HashMap<Utf8PathBuf, Option<Utf8PathBuf>>> {
    let mut result = HashMap::new();
    for vcd_file in &vcd_files {
        let vcd_file = make_relative(vcd_file);
        info!("Translating types in {vcd_file:?}");
        if !vcd_file.exists() {
            warn!("Could not translate types in {vcd_file:?} because it does not exist");
            result.insert(vcd_file.clone(), None);
            continue;
        }
        let target_file = vcd_file.with_extension("translated.vcd");
        if needs_rebuild(&target_file, vec![&vcd_translator.to_path_buf(), &vcd_file])? {
            let status = Command::new(&vcd_translator)
                .arg(&vcd_file)
                .arg("-t")
                .arg(&type_file)
                .arg("-o")
                .arg(&target_file)
                .status()
                .with_context(|| "Failed to run vcd_translate")?;

            if !status.success() {
                bail!("Failed to translate {vcd_file:?}")
            }
        }
        info!("Translated VCD: {}", target_file);
        result.insert(vcd_file.clone(), Some(target_file));
    }
    Ok(result)
}
