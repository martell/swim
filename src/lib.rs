//! The build tool for the Spade programming language
//!
//! For command line parameters, run `swim --help`
//!
//! For documentation of the swim.toml format, see [crate::config::Config]
//!
//! For documentation of the swim_plugin.toml format, see [crate::plugin::config]

use camino::{Utf8Path, Utf8PathBuf};

pub mod cmdline;
mod cocotb;
pub mod config;
pub mod init;
pub mod libraries;
pub mod nextpnr;
pub mod packing;
pub mod plugin;
pub mod pnr;
pub mod preprocessing;
pub mod report;
pub mod simulation;
pub mod spade;
pub mod synth;
pub mod upload;
pub mod util;

use libraries::{Library, PathLibrary};

#[cfg(test)]
pub mod test;

pub fn compiler_dir(root_dir: impl AsRef<Utf8Path>, compiler: &Library) -> Utf8PathBuf {
    match compiler {
        Library::Git(_) => build_dir(root_dir).join("spade"),
        Library::Path(PathLibrary { path }) => path.to_path_buf(),
    }
}

pub fn compiler_state_file(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    build_dir(root_dir).join("state.ron")
}

pub fn prelude_file(compiler_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    compiler_dir.as_ref().join("prelude").join("prelude.spade")
}

pub fn stdlib_dir(compiler_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    compiler_dir.as_ref().join("stdlib")
}

pub fn build_dir(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    root_dir.as_ref().join("build")
}

pub fn libs_dir(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    build_dir(root_dir).join("libs")
}

pub fn plugin_dir(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    build_dir(root_dir).join("plugins")
}

pub fn lock_file(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    root_dir.as_ref().join("swim.lock")
}

pub fn logfile_path(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    build_dir(root_dir).join("swim.log")
}

pub fn src_dir(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    root_dir.as_ref().join("src")
}

pub fn yosys_target_json(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    build_dir(root_dir).join("hardware.json")
}

pub fn yosys_stat_file(root_dir: impl AsRef<Utf8Path>) -> Utf8PathBuf {
    build_dir(root_dir).join("yosys-stat.txt")
}

/// Returns the path to the venv which will contain the spade python lib
pub fn venv_path(root_dir: &Utf8Path) -> Utf8PathBuf {
    build_dir(root_dir).join(".env")
}
