# Swim

A build tool for the spade programming language. Manages rebuilds of the spade
source code, compiler and any additional verilog. Supports simulation using
icarus verilog and synthesis for ECP5 and iCE40 using yosys and nextpnr. The generated
verilog can also of course be used with any other tool.

## Installation

Swim is installed using the rust package manager cargo. To install it, follow
the instructions at `rustup.rs`

Once cargo is installed, you can install the latest development version of swim by
running `cargo install --git https://gitlab.com/spade-lang/swim`.

## Usage

To create a new project, call `swim init <project name>`. It will initialise a
basic project which for you in which you can play around with spade.

To compile the spade code to verilog, run `swim build` or `swim b`. This will
build the spade compiler, then compile your code to `build/spade.sv`

### Simulation and test benches

Swim supports running test benches for your code. Before you do so, you must
add a few lines to `swim.toml`

```
[simulation]
testbench_dir = "test"
```

Test benches are currently written in Verilog, place your test benches in a
directory called `test`. Swim will build and run each Verilog file in that
directory separately, and if the exit code of the simulator is 0, the test is
considered successful.

Finally, run `swim test` to test your code.


### Synthesis, place and route

Swim can also simulate and synthesise your project using yosys and nextpnr.
Ensure those are installled, then add sections for `[synthesis]` and `[pnr]` to
your config file. The exact options you need to specify depend on the architecture, but swim should tell you which fields you need to set. As an example, here is the synthesis configuration for an ECP5 based FPGA


```
[synthesis]
top = "e_main"
extra_verilog = []
command = "synth_ecp5"

[pnr]
architecture = "ecp5"
device = "LFE5UM-85F"
pin_file = "pins.lpf"
package = "CABGA381"
```

To synthesise your code, call `swim synth` and to place and route, call `swim pnr`. Swim will make sure the prerequisite steps are performed for you, so if your end goal is pnr, you can call `swim pnr` directly.

### Upload

Swim can also upload your code for a few FPGAs. To get started, add an
`[upload]` section to your config. Like synthesis, the exact options depend on
your target, so let the error messsages from swim guide the configuration.

To upload, call `swim upload`

### Templates

If you're using a supported board you can copy a template repository which
contains a project that's ready to upload. Check available boards with
`swim init --list-boards` and then `swim init --board <board>`.


## A note on the spade compiler and submodules

As spade is still early in development, it is useful to have each project pinned
to a specific compiler versoin, rather than having a global copy of the
compiler. This means that it will still build in the future even if breaking
changes are made to the language.

By default, swim tracks the compiler version in a file called `swim.lock` that
is created on the first build. It is probably a good idea to track this using
git or another VCS. If you want to update to the newest version of the spade
compiler, run `swim update-spade` and commit the updated `swim.lock`.

If you prefer keeping your own submodule (perhaps you want to do your own
changes to the compiler?), you can also setup a path dependency and track it
like any other submodule. For example:

```sh
git submodule add https://gitlab.com/spade-lang/spade.git spade
git commit -m "Add spade submodule"
```

And then, instruct swim to use a path to the compiler instead by changing your
swim.toml to this:

```toml
compiler = { path = "spade" }
```

### Using another compiler branch

You can depend on a specific branch by setting the `compiler`-field in your
`swim.toml`:

```toml
compiler = { git = "https://gitlab.com/spade-lang/spade.git", branch = "another-branch" }
```

After setting the field, run `swim update-spade` to update the pinned compiler.

You can also change the repository, if you wish.

### Using a global compiler

If you prefer using a global compiler, you can set the `compiler`-field to point
to an absolute path:

```toml
compiler = { path = "/path/to/spade" }
```

## License

Swim is licensed under the [EUPL-1.2 license](LICENSE.txt).
